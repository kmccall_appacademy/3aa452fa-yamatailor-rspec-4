class Temperature

  def initialize(options = {})
    @temp = options
  end

  def in_celsius
    @temp[:f] ? ((@temp[:f] - 32) / 1.8).round : @temp[:c]
  end

  def in_fahrenheit
    @temp[:c] ? @temp[:c] * 1.8 + 32 : @temp[:f]
  end

  def self.from_celsius(celsius)
    Temperature.new(c: celsius)
  end

  def self.from_fahrenheit(fahrenheit)
    Temperature.new(f: fahrenheit)
  end
end

class Celsius < Temperature

  def initialize(celsius)
    @temp = {c: celsius}
  end
end

class Fahrenheit < Temperature

  def initialize(fahrenheit)
    @temp = {f: fahrenheit}
  end
end
