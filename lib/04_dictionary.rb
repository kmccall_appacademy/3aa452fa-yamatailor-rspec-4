class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    class_type = entry.class

    if class_type == String
      @entries[entry] = nil
    end

    if class_type == Hash
      entry.each { |k,v| @entries[k] = v }
    end
  end

  def keywords
    @entries.keys.reverse
  end

  def include?(keyword)
    @entries.include?(keyword) ? true : false
  end

  def find(keyword)
    array = []
    @entries.each do |arr|
      if arr.first == keyword || arr.first.include?(keyword)
        array << arr
      end
    end
    array.to_h
  end

  def printable
    arr = ""
    keywords.each do |k, _v|
      last = keywords.last
      if last == k
        arr << "[#{k}] \"#{@entries[k]}\""
        break
      end
      arr << "[#{k}] \"#{@entries[k]}\"\n"
    end
    arr
  end
end
