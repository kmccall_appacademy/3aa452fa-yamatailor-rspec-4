class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = "%.2d" % (@seconds / 3600)
    minutes = "%.2d" % ((@seconds / 60) % 60)
    seconds = "%.2d" % (@seconds % 60)
    "#{hours}:#{minutes}:#{seconds}"
  end
end
