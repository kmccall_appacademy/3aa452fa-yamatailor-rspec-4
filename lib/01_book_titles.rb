class Book
  attr_accessor :book
  attr_reader :title

  def title=(title)
    @title = capitalization(title)
  end

  def capitalization(title)
    lower_case = %w(and an the in of a)

    arr = []
    title.split.each_with_index do |word, i|
      if i == 0
        arr << word.capitalize
        next
      end
      lower_case.include?(word) ? arr << word : arr << word.capitalize
    end
    arr.join(" ")
  end
end
